package models

import _ "github.com/jinzhu/gorm"

type Focus struct {
	Id        int    `json:"id" form:"id"`
	Title     string `json:"title" form:"title"`
	FocusType int    `json:"focus_type" form:"focus_type"`
	FocusImg  string `json:"focus_img" form:"focus_img"`
	Link      string `json:"link" form:"link"`
	Sort      int    `json:"sort" form:"sort"`
	Status    int    `json:"status" form:"status"`
	AddTime   int    `json:"add_time" form:"add_time"`
}

func (Focus) TableName() string {
	return "focus"
}
