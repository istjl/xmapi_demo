package models

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"github.com/astaxie/beego"
	. "github.com/hunterhug/go_image"
	"path"
	"strconv"
	"strings"
	"time"
)

func UnixToDate(timestamp int) string {

	t := time.Unix(int64(timestamp), 0)

	return t.Format("2006-01-02 15:04:05")
}

//2020-05-02 15:04:05
func DateToUnix(str string) int64 {
	template := "2006-01-02 15:04:05"
	t, err := time.ParseInLocation(template, str, time.Local)
	if err != nil {
		beego.Info(err)
		return 0
	}
	return t.Unix()
}

func GetUnix() int64 {
	return time.Now().Unix()
}
func GetUnixNano() int64 {
	return time.Now().UnixNano()
}
func GetDate() string {
	template := "2006-01-02 15:04:05"
	return time.Now().Format(template)
}

func Md5(str string) string {
	m := md5.New()
	m.Write([]byte(str))
	return string(hex.EncodeToString(m.Sum(nil)))
}

func Hello(in string) (out string) {
	out = in + "world"
	return
}
func GetDay() string {
	template := "20060102"
	return time.Now().Format(template)
}
func ResizeImage(filename []string) {
	resizeImageSize := strings.Split(beego.AppConfig.String("ResizeImageSize"), ",")
	fmt.Println(resizeImageSize)
	extName := path.Ext(filename[0])
	for i := 0; i < len(resizeImageSize); i++ {
		w := resizeImageSize[i]
		width, _ := strconv.Atoi(w)
		savepath := filename[0] + "_" + w + "x" + w + extName
		err := ThumbnailF2F(filename[0], savepath, width, width)

		if err != nil {
			beego.Error(err)
		}
	}

}
