package models

type RoleAccess struct {
	RoleId   int `json:"role_id"`
	AccessId int `json:"access_id"`
}

func (RoleAccess) TableName() string {
	return "role_access"

}
