package models

type Res struct {
	Code    int                    `json:"code"`
	Content string                 `json:"content"`
	Data    map[string]interface{} `json:"data"`
}
