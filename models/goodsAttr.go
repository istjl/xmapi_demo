package models

type GoodsAttr struct {
	Id              int    `json:"id"`
	GoodsId         int    `json:"goods_id"`
	AttributeCateId int    `json:"attribut_cate_id"`
	AttributeId     int    `json:"attribut_id"`
	AttributeTitle  string `json:"attribute_title"`
	AttributeType   int    `json:"attribut_type"`
	AttributeValue  string `json:"attribute_value"`
	Sort            int    `json:"sort"`
	AddTime         int    `json:"add_time"`
	Status          int    `json:"status"`
}

func (GoodsAttr) TableName() string {
	return "goods_attr"
}
