package models

const (
	StatusOk                  = 200
	StatusMultipleChoices     = 300
	StatusNotFound            = 404
	StatusInternalServerError = 500
)

