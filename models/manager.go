package models

import _ "github.com/jinzhu/gorm"

type Manager struct {
	Id       int    `json:"id"`
	Username string `json:"username" form:"username"`
	Password string `json:"password" form:"password"`
	Mobile   string `json:"mobile"`
	Email    string `json:"email"`
	Status   int    `json:"status"`
	RoleId   int    `json:"role_id"`
	AddTime  int    `json:"add_time"`
	IsSuper  int    `json:"is_super"`
	Role     Role   `gorm:"foreignkey:Id;association_foreignkey:RoleId"`
}

func (Manager) TableName() string {
	return "manager"
}
