package models

type Access struct {
	Id          int      `json:"id"`
	ModuleName  string   `json:"module_name"` //模块名称
	ActionName  string   `json:"action_name"` //操作名称
	Type        int      `json:"type"`        //节点类型: 1表示模块 2表示菜单 3操作
	Url         string   `json:"url"`         //路由跳转地址
	ModuleId    int      `json:"module_id"`   //此module_id 和当前模型的_id 关联    module_id 0 表示模块
	Sort        int      `json:"sort"`
	Description string   `json:"description"`
	AddTime     int      `json:"add_time"`
	Status      int      `json:"status"`
	AccessItem  []Access `gorm:"ForeignKey:ModuleId;AssociationForeignKey:Id"`
	Checked     bool     `sql:"-" grom:"_"`
}

func (Access) TableName() string {
	return "access"
}
