package models

type Goods struct {
	Id            int     `json:"id" form:"id"`
	Title         string  `json:"title" form:"title"`
	SubTitle      string  `json:"sub_title" form:"sub_title"`
	GoodsSn       string  `json:"goods_sn" form:"goods_sn"`
	CateId        int     `json:"cate_id" form:"cate_id"`
	ClickCount    int     `json:"click_count" form:"click_count"`
	GoodsNumber   int     `json:"goods_number" form:"goods_number"`
	Price         float64 `json:"price" form:"price"`
	MarketPrice   float64 `json:"market_price" form:"market_price"`
	RelationGoods string  `json:"relation_goods" form:"relation_goods"`
	GoodsAttrs    string  `json:"goods_attrs" form:"goods_attr"`
	GoodsColors   string  `json:"goods_colors" form:"goods_colors"`
	GoodsVersion  string  `json:"goods_version" from:"goods_version"`
	GoodsImg      string  `json:"goods_img" form:"goods_img"`
	GoodsGift     string  `json:"goods_gift" from:"goods_gift"`
	GoodsFitting  string  `json:"goods_fitting" from:"goods_fitting"`
	GoodsKeywords string  `json:"goods_keywords" form:"goods_keywords"`
	GoodsDesc     string  `json:"goods_desc" form:"goods_desc"`
	Content       string  `json:"content" form:"goods_content"`
	IsDelete      int     `json:"is_delete" form:"is_delete"`
	IsHot         int     `json:"is_hot" form:"is_hot"`
	IsBest        int     `json:"is_best" form:"is_best"`
	IsNew         int     `json:"is_new" form:"is_new"`
	GoodsTypeId   int     `json:"goods_type_id" form:"goods_type_id"`
	Sort          int     `json:"sort" form:"sort"`
	Status        int     `json:"status" form:"status"`
}

func (Goods) TableName() string {
	return "goods"
}
