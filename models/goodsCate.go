package models

import _ "github.com/jinzhu/gorm"

type GoodsCate struct {
	Id            int         `json:"id"`
	Title         string      `json:"title"`
	CateImg       string      `json:"cate_img"`
	Link          string      `json:"link"`
	Template      string      `json:"template"`
	Pid           int         `json:"pid"`
	SubTitle      string      `json:"sub_title"`
	Keywords      string      `json:"keywords"`
	Description   string      `json:"description"`
	Status        int         `json:"status"`
	Sort          int         `json:"sort"`
	AddTime       int         `json:"add_time"`
	GoodsCateItem []GoodsCate `gorm:"ForeignKey:Pid;AssociationForeignKey:Id"`
}

func (GoodsCate) TableName() string {
	return "goods_cate"
}
