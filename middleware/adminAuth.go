package middleware

import (
	context2 "github.com/astaxie/beego/context"
	"net/url"
	"strings"
	"xmapi_demo/models"
)

func AdminAuth(c *context2.Context) {
	pathname := c.Request.URL.String()
	//获取session
	userinfo, ok := c.Input.Session("userinfo").(models.Manager)
	if !(ok && userinfo.Username != "") {
		if pathname != "/admin/login" && pathname != "/admin/login/doLogin" {
			return
		}
	} else {
		/*
			1、判断管理员是不是超级管理员
		*/
		if userinfo.IsSuper != 1 {
			roleId := userinfo.RoleId
			roleAccess := []models.RoleAccess{}
			models.DB.Where("role_id = ?", roleId).Find(&roleAccess)
			roleAccessMap := make(map[int]int)
			for _, v := range roleAccess {
				roleAccessMap[v.AccessId] = v.AccessId
			}
			pathname = strings.Replace(pathname, "/admin", "", 1)

			urlObj, _ := url.Parse(pathname)
			access := models.Access{}
			models.DB.Where("url = ?", urlObj.Path).Find(&access)

			//3 判断当前访问的url对应的权限id,是否在权限列表的id中
			if _, ok := roleAccessMap[access.Id]; ok {
				c.WriteString("没有权限")
				return
			}
		}
	}
}
