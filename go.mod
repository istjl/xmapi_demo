module xmapi_demo

go 1.13

require (
	github.com/aliyun/aliyun-oss-go-sdk v2.1.3+incompatible
	github.com/astaxie/beego v1.12.2
	github.com/hunterhug/go_image v0.0.0-20190710020854-8922226c5f4b
	github.com/jinzhu/gorm v1.9.14
	github.com/mojocn/base64Captcha v1.3.1
	github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e
	github.com/smartystreets/goconvey v1.6.4
	golang.org/x/time v0.0.0-20200630173020-3af7569d3a1e // indirect
)
