package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["xmapi_demo/controllers/goods:ColorController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/goods:ColorController"],
		beego.ControllerComments{
			Method:           "Get",
			Router:           "/getColor",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsCateController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsCateController"],
		beego.ControllerComments{
			Method:           "Add",
			Router:           "/goodsCate/add",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsCateController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsCateController"],
		beego.ControllerComments{
			Method:           "Delete",
			Router:           "/goodsCate/delete",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsCateController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsCateController"],
		beego.ControllerComments{
			Method:           "Edit",
			Router:           "/goodsCate/edit",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsCateController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsCateController"],
		beego.ControllerComments{
			Method:           "Get",
			Router:           "/goodsCate/get",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsCateController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsCateController"],
		beego.ControllerComments{
			Method:           "GetList",
			Router:           "/goodsCate/getlist",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsController"],
		beego.ControllerComments{
			Method:           "Recommend",
			Router:           "/Recommend",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsController"],
		beego.ControllerComments{
			Method:           "Add",
			Router:           "/add",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsController"],
		beego.ControllerComments{
			Method:           "Count",
			Router:           "/count",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsController"],
		beego.ControllerComments{
			Method:           "Delete",
			Router:           "/delete",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsController"],
		beego.ControllerComments{
			Method:           "Edit",
			Router:           "/edit",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsController"],
		beego.ControllerComments{
			Method:           "Get",
			Router:           "/get",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsController"],
		beego.ControllerComments{
			Method:           "UploadImage",
			Router:           "/upload",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsController"],
		beego.ControllerComments{
			Method:           "UploadImages",
			Router:           "/uploads",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsTypeAttrController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsTypeAttrController"],
		beego.ControllerComments{
			Method:           "Add",
			Router:           "/goodsTypeAttr/add",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsTypeAttrController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsTypeAttrController"],
		beego.ControllerComments{
			Method:           "Delete",
			Router:           "/goodsTypeAttr/delete",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsTypeAttrController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsTypeAttrController"],
		beego.ControllerComments{
			Method:           "Edit",
			Router:           "/goodsTypeAttr/edit",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsTypeAttrController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsTypeAttrController"],
		beego.ControllerComments{
			Method:           "Get",
			Router:           "/goodsTypeAttr/get",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsTypeAttrController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsTypeAttrController"],
		beego.ControllerComments{
			Method:           "GetCate",
			Router:           "/goodsTypeAttr/getCate",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsTypeController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsTypeController"],
		beego.ControllerComments{
			Method:           "Add",
			Router:           "/goodsType/add",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsTypeController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsTypeController"],
		beego.ControllerComments{
			Method:           "Delete",
			Router:           "/goodsType/delete",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsTypeController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsTypeController"],
		beego.ControllerComments{
			Method:           "Edit",
			Router:           "/goodsType/edit",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsTypeController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/goods:GoodsTypeController"],
		beego.ControllerComments{
			Method:           "Get",
			Router:           "/goodsType/get",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

}
