package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:AccessController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:AccessController"],
		beego.ControllerComments{
			Method:           "Add",
			Router:           "/access/add",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:AccessController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:AccessController"],
		beego.ControllerComments{
			Method:           "Delete",
			Router:           "/access/delete",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:AccessController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:AccessController"],
		beego.ControllerComments{
			Method:           "Edit",
			Router:           "/access/edit",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:AccessController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:AccessController"],
		beego.ControllerComments{
			Method:           "Get",
			Router:           "/access/get",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:AccessController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:AccessController"],
		beego.ControllerComments{
			Method:           "GetList",
			Router:           "/access/getlist",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:CaptchaController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:CaptchaController"],
		beego.ControllerComments{
			Method:           "Get",
			Router:           "/captcha",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:CaptchaController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:CaptchaController"],
		beego.ControllerComments{
			Method:           "Post",
			Router:           "/captcha",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:FocusController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:FocusController"],
		beego.ControllerComments{
			Method:           "Add",
			Router:           "/focus/add",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:FocusController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:FocusController"],
		beego.ControllerComments{
			Method:           "Delete",
			Router:           "/focus/delete",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:FocusController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:FocusController"],
		beego.ControllerComments{
			Method:           "Edit",
			Router:           "/focus/edit",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:FocusController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:FocusController"],
		beego.ControllerComments{
			Method:           "Get",
			Router:           "/focus/get",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:LoginControllers"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:LoginControllers"],
		beego.ControllerComments{
			Method:           "CaptCha",
			Router:           "/captcha",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:LoginControllers"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:LoginControllers"],
		beego.ControllerComments{
			Method:           "Login",
			Router:           "/login",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:LoginControllers"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:LoginControllers"],
		beego.ControllerComments{
			Method:           "LoginOut",
			Router:           "/loginOut",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:LoginControllers"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:LoginControllers"],
		beego.ControllerComments{
			Method:           "Register",
			Router:           "/register",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:ManagerController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:ManagerController"],
		beego.ControllerComments{
			Method:           "Add",
			Router:           "/manager/add",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:ManagerController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:ManagerController"],
		beego.ControllerComments{
			Method:           "Delete",
			Router:           "/manager/delete",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:ManagerController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:ManagerController"],
		beego.ControllerComments{
			Method:           "Edit",
			Router:           "/manager/edit",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:ManagerController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:ManagerController"],
		beego.ControllerComments{
			Method:           "Get",
			Router:           "/manager/get",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:NavController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:NavController"],
		beego.ControllerComments{
			Method:           "Add",
			Router:           "/nav/add",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:NavController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:NavController"],
		beego.ControllerComments{
			Method:           "Delete",
			Router:           "/nav/delete",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:NavController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:NavController"],
		beego.ControllerComments{
			Method:           "Edit",
			Router:           "/nav/edit",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:NavController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:NavController"],
		beego.ControllerComments{
			Method:           "Get",
			Router:           "/nav/get",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:RoleController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:RoleController"],
		beego.ControllerComments{
			Method:           "Add",
			Router:           "/role/add",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:RoleController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:RoleController"],
		beego.ControllerComments{
			Method:           "Auth",
			Router:           "/role/auth",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:RoleController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:RoleController"],
		beego.ControllerComments{
			Method:           "Delete",
			Router:           "/role/delete",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:RoleController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:RoleController"],
		beego.ControllerComments{
			Method:           "Edit",
			Router:           "/role/edit",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:RoleController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:RoleController"],
		beego.ControllerComments{
			Method:           "Get",
			Router:           "/role/get",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:RoleController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:RoleController"],
		beego.ControllerComments{
			Method:           "GetAuth",
			Router:           "/role/getAuth",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:SettingController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:SettingController"],
		beego.ControllerComments{
			Method:           "Edit",
			Router:           "/setting/edit",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/admin:SettingController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/admin:SettingController"],
		beego.ControllerComments{
			Method:           "Get",
			Router:           "/setting/get",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

}
