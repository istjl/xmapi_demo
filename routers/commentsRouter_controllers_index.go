package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["xmapi_demo/controllers/index:IndexController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/index:IndexController"],
		beego.ControllerComments{
			Method:           "Get",
			Router:           "/index/index",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers/index:UserController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers/index:UserController"],
		beego.ControllerComments{
			Method:           "Get",
			Router:           "/get",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

}
