// @APIVersion 1.0.0
// @Title beego Test API
// @Description beego has a very cool tools to autogenerate documents for your API
// @Contact astaxie@gmail.com
// @TermsOfServiceUrl http://beego.me/
// @License Apache 2.0
// @LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html
package routers

import (
	"github.com/astaxie/beego"
	"xmapi_demo/controllers"
	"xmapi_demo/controllers/admin"
	"xmapi_demo/controllers/goods"
	"xmapi_demo/controllers/index"
	"xmapi_demo/middleware"
)

func init() {
	ns := beego.NewNamespace("/v1",
		beego.NSNamespace("/index",
			beego.NSInclude(
				&index.UserController{},
			),
			beego.NSInclude(
				&index.ProductController{},
			),
		),
		beego.NSNamespace("/public",
			beego.NSInclude(
				&controllers.MainController{},
			),
			beego.NSInclude(
				&index.IndexController{},
			),
			beego.NSInclude(
				&admin.CaptchaController{},
			),
		),
		beego.NSNamespace("/goods",
			beego.NSInclude(
				&goods.GoodsController{},
			),
			beego.NSInclude(
				&goods.ColorController{},
			), beego.NSInclude(
				&goods.GoodsCateController{},
			),
			beego.NSInclude(
				&goods.GoodsTypeAttrController{},
			),
			beego.NSInclude(
				&goods.GoodsTypeController{},
			),
		),
		beego.NSNamespace("/admin",
			beego.NSBefore(middleware.AdminAuth),

			beego.NSInclude(
				&admin.LoginControllers{},
			),
			beego.NSInclude(
				&admin.RoleController{},
			),
			beego.NSInclude(
				&admin.ManagerController{},
			),
			beego.NSInclude(
				&admin.AccessController{},
			),
			beego.NSInclude(
				&admin.FocusController{},
			),
			beego.NSInclude(
				&admin.NavController{},
			),
			beego.NSInclude(
				&admin.SettingController{},
			),
		),
	)
	beego.AddNamespace(ns)
}
