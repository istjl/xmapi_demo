package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["xmapi_demo/controllers:MainController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers:MainController"],
		beego.ControllerComments{
			Method:           "EditNum",
			Router:           "/changeNum",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["xmapi_demo/controllers:MainController"] = append(beego.GlobalControllerRouter["xmapi_demo/controllers:MainController"],
		beego.ControllerComments{
			Method:           "ChangeStatus",
			Router:           "/changeStatus",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

}
