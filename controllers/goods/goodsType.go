package goods

import (
	"xmapi_demo/controllers"
	"xmapi_demo/models"
)

type GoodsTypeController struct {
	controllers.BaseController
}

// @Title Get
// @Description 获取分类数据
// @Params id query int false "分类id"
// @Success 200 {object} models.GoodsType
// @Failure 503 ""
// @router /goodsType/get [post]
func (c *GoodsTypeController) Get() {
	id, _ := c.GetInt("id")
	roles := []models.GoodsType{}
	if id != 0 {
		role := models.GoodsType{Id: id}
		models.DB.Find(&role)
		c.SuccessJson(role)
	} else {
		err := models.DB.Find(&roles).Error
		if err != nil {
			c.ErrorJson(503, "获取分类失败", nil)
		} else {
			c.SuccessJson(roles)
		}
	}
}

// @Title Add
// @Description 新增分类信息
// @Params title query string true "分类title"
// @Params description query string true "分类描述"
// @Success 200 {object} models.GoodsType
// @Failure 503 "新增失败"
// @router /goodsType/add [post]
func (c *GoodsTypeController) Add() {
	title := c.GetString("title")
	description := c.GetString("description")
	goodsType := models.GoodsType{
		Title:       title,
		Description: description,
		Status:      1,
		AddTime:     int(models.GetUnix()),
	}
	err := models.DB.Save(&goodsType).Error
	if err != nil {
		c.ErrorJson(403, "新增分类失败", nil)
		return
	}
	c.SuccessJson(goodsType)
}

// @Title Edit
// @Description 修改分类信息
// @params id query int true '需要修改分类的id'
// @Params title query string false "分类title"
// @Params description query string false "分类描述"
// @Params status query int true '分类状态 1.显示 0.隐藏'
// @Success 200 {object} models.GoodsType
// @Failure 503 "新增失败"
// @router /goodsType/edit [post]
func (c *GoodsTypeController) Edit() {
	id, err := c.GetInt("id")
	title := c.GetString("title")
	description := c.GetString("description")
	status, _ := c.GetInt("status")
	if err != nil {
		c.ErrorJson(403, "传入参数错误", nil)
		return
	}
	goodsType := models.GoodsType{Id: id}
	err1 := models.DB.Find(&goodsType).Error
	if err1 != nil {
		c.ErrorJson(403, "查询失败", nil)
		return
	}
	if title != "" {
		goodsType.Title = title
	}
	if description != "" {
		goodsType.Description = description
	}
	goodsType.Status = status
	err2 := models.DB.Save(&goodsType).Error
	if err2 != nil {
		c.ErrorJson(403, "更新数据失败", nil)
		return
	}
	c.SuccessJson(goodsType)
}

// @Title Delete
// @Description 删除商品分类
// @params id query int true '需要删除分类的id'
// @Success 200 {string}删除成功
// @Failure 503 "删除失败"
// @router /goodsType/delete [post]
func (c *GoodsTypeController) Delete() {
	id, err := c.GetInt("id")
	if err != nil {
		c.ErrorJson(403, "传入参数错误", nil)
		return
	}
	goodsType := models.GoodsType{Id: id}
	err1 := models.DB.Delete(&goodsType).Error
	if err1 != nil {
		c.ErrorJson(403, "删除失败", nil)
		return
	}
	c.SuccessJson("删除成功")
}
