package goods

import (
	"fmt"
	"strconv"
	"strings"
	. "sync"
	"xmapi_demo/controllers"
	"xmapi_demo/models"
)

type GoodsController struct {
	controllers.BaseController
}

var wg WaitGroup

// @Title Get
// @Description 获取商品数据
// @Params id query int false "分类id"
// @Params page query int true "页数"
// @Params limit query int true "一页显示几条"
// @Params keyword query string false "关键字"
// @Success 200 {object} models.Goods
// @Failure 503 "获取数据失败"
// @router /get [post]
func (c *GoodsController) Get() {
	id, _ := c.GetInt("id")
	page, _ := c.GetInt("page")
	limit, _ := c.GetInt("limit")
	keyword := c.GetString("keyword")
	if id != 0 {
		good := models.Goods{Id: id}
		models.DB.Find(&good)
		c.SuccessJson(good)
		return
	}
	if page == 0 {
		page = 1
	}
	goods := []models.Goods{}
	where := "1=1"
	if len(keyword) > 0 {
		where += " AND title like \"%" + keyword + "%\""
	}
	err := models.DB.Where(where).Limit(limit).Offset((page - 1) * limit).Find(&goods).Error
	if err != nil {
		c.ErrorJson(403, "获取数据失败", nil)
		return
	}
	c.SuccessJson(goods)
}

// @Title Count
// @Description 统计数量
// @Success 200 {number}
// @Failure 503 "获取数据失败"
// @router /count [post]
func (c *GoodsController) Count() {
	var count int
	models.DB.Table("goods").Count(&count)
	c.SuccessJson(count)
}

// @Title Add
// @Description 新增商品
// @Params title query string true  "商品标题"
// @Params sub_title query string true "商品的二级标题"
// @Params goods_sn query string true "商品的sn号"
// @Params cate_id query int true "商品所属分类"
// @Params goods_number query int true "商品库存"
// @Params market_price query float true "市场价"
// @Params price query float true "价格"
// @Params relation_goods query string true "关联的商品"
// @Params goods_attr query string  true "商品的属性"
// @Params goods_version query string true "商品版本号"
// @Params goods_gift query string true  "商品赠品"
// @Params goods_fitting query string  true "商品的配件"
// @Params goods_color query string true "商品关联的颜色"
// @Params goods_keywords query string true "商品关键字"
// @Params goods_desc query string true "商品描述"
// @Params goods_content query string true"商品详情"
// @Params is_delete query int true "是否删除"
// @Params is_hot query int true "是否热销"
// @Params is_best query int true "是否最好的"
// @Params is_new query int true "是否新品"
// @Params goods_type_id query int true "关联商品的id"
// @Params sort query int true "排序"
// @Params status query int true "状态"
// @Params goods_img query file true "商品图片"
// @Params attr_id_list query string true "规格包装"
// @Success 200 {object} models.GoodsType
// @Failure 503 ""
// @router /add [post]
func (c *GoodsController) Add() {
	goodsVersion := c.GetString("goods_version")
	goodsGift := c.GetString("goods_gift")
	goodsFitting := c.GetString("goods_fitting")
	goodsImg, err2 := c.UploadImgs("goods_img")
	if err2 == nil && len(goodsImg) > 0 {
		wg.Add(1)
		go func() {
			models.ResizeImage(goodsImg)
			wg.Done()
		}()
	}
	goods := models.Goods{
		GoodsVersion: goodsVersion,
		GoodsGift:    goodsGift,
		GoodsFitting: goodsFitting,
	}
	c.ParseForm(&goods)
	fmt.Println(goods)
	err := models.DB.Create(&goods).Error
	if err != nil {
		c.ErrorJson(403, "新建数据失败", nil)
		return
	}
	wg.Add(1)
	go func() {
		for _, v := range goodsImg {
			goodsImgObj := models.GoodsImage{}
			goodsImgObj.GoodsId = goods.Id
			goodsImgObj.ImgUrl = v
			goodsImgObj.Sort = 10
			goodsImgObj.Status = 1
			goodsImgObj.AddTime = int(models.GetUnix())
			fmt.Println(goodsImgObj)
			models.DB.Create(&goodsImgObj)
		}
		wg.Done()
	}()

	wg.Add(1)
	go func() {
		attrIdList := c.GetString("attr_id_list")
		arr := strings.Split(attrIdList, ",")
		for k, _ := range arr {
			goodsTypeAttributeId, _ := strconv.Atoi(arr[k])
			goodsTypeAttributeObj := models.GoodsTypeAttribute{Id: goodsTypeAttributeId}
			goodsAttrObj := models.GoodsAttr{}
			models.DB.Find(&goodsTypeAttributeObj)
			goodsAttrObj.GoodsId = goods.Id
			goodsAttrObj.AttributeTitle = goodsTypeAttributeObj.Title
			goodsAttrObj.AttributeType = goodsTypeAttributeObj.AttrType
			goodsAttrObj.AttributeCateId = goodsTypeAttributeObj.CateId
			goodsAttrObj.AttributeValue = arr[k]
			goodsAttrObj.Status = 1
			goodsAttrObj.Sort = 10
			goodsAttrObj.AddTime = int(models.GetUnix())
			fmt.Println(goodsAttrObj, "obj")
			models.DB.Create(&goodsAttrObj)
		}
		wg.Done()
	}()
	wg.Wait()
	fmt.Println(models.GetUnix())
	c.SuccessJson(goods)
}

// @Title Edit
// @Description 修改商品
// @Params id query int false "商品id"
// @Params title query string false  "商品标题"
// @Params sub_title query string false "商品的二级标题"
// @Params goods_sn query string false "商品的sn号"
// @Params cate_id query int false "商品所属分类"
// @Params goods_number query int false "商品库存"
// @Params market_price query float false "市场价"
// @Params price query float false "价格"
// @Params relation_goods query string false "关联的商品"
// @Params goods_attr query string  false "商品的属性"
// @Params goods_version query string false "商品版本号"
// @Params goods_gift query string false  "商品赠品"
// @Params goods_fitting query string  false "商品的配件"
// @Params goods_color query string false "商品关联的颜色"
// @Params goods_keywords query string false "商品关键字"
// @Params goods_desc query string false "商品描述"
// @Params goods_content query string false"商品详情"
// @Params is_delete query int false "是否删除"
// @Params is_hot query int false "是否热销"
// @Params is_best query int false "是否最好的"
// @Params is_new query int false "是否新品"
// @Params goods_type_id query int false "关联商品的id"
// @Params sort query int false "排序"
// @Params status query int false "状态"
// @Success 200 {object} models.GoodsType
// @Failure 503 ""
// @router /edit [post]
func (c *GoodsController) Edit() {
	id, err1 := c.GetInt("id")

	if err1 != nil {
		c.ErrorJson(403, "传入参数错误", nil)
		return
	}
	goods := models.Goods{Id: id}
	models.DB.Find(&goods)
	err := c.ParseForm(&goods)
	if err != nil {
		c.ErrorJson(403, "传入参数错误", nil)
		return
	}
	err2 := models.DB.Save(&goods).Error
	if err2 != nil {
		c.ErrorJson(403, "修改失败", nil)
		return
	}
	c.SuccessJson(goods)
}

// @Title Delete
// @Description 删除商品
// @Params id query int false "商品id"
// @Success 200 {object} models.GoodsType
// @Failure 503 ""
// @router /delete [post]
func (c *GoodsController) Delete() {
	id, err := c.GetInt("id")
	if err != nil {
		c.ErrorJson(403, "传入参数失败", nil)
		return
	}
	goods := models.Goods{Id: id}
	err2 := models.DB.Delete(&goods).Error
	if err2 != nil {
		c.ErrorJson(403, "删除失败", nil)
	}
	goodsAttr := models.GoodsAttr{}
	goodsImage := models.GoodsImage{}
	models.DB.Where("goods_id = ?", id).Delete(&goodsAttr)
	models.DB.Where("goods_id = ?", id).Delete(&goodsImage)
	c.SuccessJson("删除成功")
}

// @Title UploadImage
// @Description 上传图片
// @Params file query file true "上传图片"
// @Success 200 {string} "上传成功"
// @Failure 503 ""
// @router /upload [post]
func (c *GoodsController) UploadImage() {
	savePath, err := c.UploadImg("file")
	if err != nil {
		c.ErrorJson(403, "上传失败", nil)
		return
	}
	fmt.Println(savePath)
	c.Data["json"] = map[string]interface{}{
		"link": savePath,
	}
	c.ServeJSON()
}

// @Title UploadImages
// @Description 上传图片
// @Params file query file true "上传图片"
// @Success 200 {string} "上传成功"
// @Failure 503 ""
// @router /uploads [post]
func (c *GoodsController) UploadImages() {
	savePath, err := c.UploadImgs("file")
	if err != nil {
		c.ErrorJson(403, "上传失败", nil)
		return
	}
	c.SuccessJson(savePath)
}

// @Title Recommend
// @Description 获取推荐商品
// @Params cate_id query int true "分类id"
// @Params goods_type query string "hot,best,new"
// @Params limit query int "数量"
// @Success 200 {object} models.Goods
// @Failure 503 ""
// @router /Recommend [post]
func (c *GoodsController) Recommend() {
	cate, _ := c.GetInt("cate_id")
	goods_type := c.GetString("goods_type")
	limit, _ := c.GetInt("limit")
	where := "cate_id = ?"
	switch goods_type {
	case "hot":
		where += " And is_hota=1"
	case "best":
		where += " And is_best=1"
	case "new":
		where += " And is_new=1"
	default:
		break
	}
	goods := models.Goods{}
	models.DB.Where(where, cate).Find(&goods).Limit(limit)
	c.SuccessJson(goods)
}
