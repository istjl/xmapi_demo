package goods

import (
	"xmapi_demo/controllers"
	"xmapi_demo/models"
)

type GoodsCateController struct {
	controllers.BaseController
}

// @Title Get
// @Description 获取商品
// @Params id query int false "二级分类id"
// @Success 200 {object} models.GoodsCate
// @Failure 403 获取商品失败
// @router /goodsCate/get [post]
func (c *GoodsCateController) Get() {
	id, _ := c.GetInt("id")
	if id != 0 {
		GoodsCate := models.GoodsCate{Id: id}
		err1 := models.DB.Find(&GoodsCate).Error
		if err1 != nil {
			c.ErrorJson(503, "获取失败", nil)
			return
		}
		c.SuccessJson(GoodsCate)
		return
	}
	GoodsCate := []models.GoodsCate{}
	err := models.DB.Preload("GoodsCateItem").Where("pid = 0").Find(&GoodsCate).Error
	if err != nil {
		c.ErrorJson(503, "获取商品失败", nil)
		return
	}
	c.SuccessJson(GoodsCate)
}

// @Title GetList
// @Description 获取关联数据
// @Success 200 {object} models.Access
// @Failure 403 获取数据失败
// @router /goodsCate/getlist [post]
func (c *GoodsCateController) GetList() {
	GoodsCate := []models.GoodsCate{}
	err := models.DB.Preload("GoodsCateItem").Where("pid = 0").Find(&GoodsCate).Error
	if err != nil {
		c.ErrorJson(403, "获取数据失败", nil)
		return
	}
	c.SuccessJson(GoodsCate)
}

// @Title Add
// @Description 新增商品
// @Params title query string true "分类的title"
// @Params pid query int true "分类id  0.顶级分类，其他 二级分类"
// @Params link query string  false "分类跳转地址"
// @Params template query string false "分类模板"
// @Params sub_title query string false  "seo标题"
// @Params keywords query string false "seo关键字"
// @Params description query string false "关键字描述"
// @Params sort query int false "状态"
// @Params cate_img query file false "分类图片"
// @Params status query int true "状态"
// @Success 200 {object} models.GoodsCate
// @Failure 403 增加数据失败
// @router /goodsCate/add [post]
func (c *GoodsCateController) Add() {
	title := c.GetString("title")
	pid, err1 := c.GetInt("pid")
	link := c.GetString("link")
	template := c.GetString("template")
	sutTitle := c.GetString("sub_title")
	keywords := c.GetString("keywords")
	description := c.GetString("description")
	sort, _ := c.GetInt("sort")
	status, err3 := c.GetInt("status")
	if err1 != nil || err3 != nil {
		c.ErrorJson(403, "传入参数错误", nil)
		return
	}
	uploadDir, err := c.UploadImg("cate_img")
	if err != nil {
		c.ErrorJson(403, "上传图片错误", nil)
	}
	goodsCate := models.GoodsCate{
		Title:   title,
		Pid:     pid,
		Status:  status,
		AddTime: int(models.GetUnix()),
	}
	if uploadDir != "" {
		goodsCate.CateImg = uploadDir
	}
	if link != "" {
		goodsCate.Link = link
	}
	if sutTitle != "" {
		goodsCate.SubTitle = sutTitle
	}
	if keywords != "" {
		goodsCate.Keywords = keywords
	}
	if description != "" {
		goodsCate.Description = description
	}
	if template != "" {
		goodsCate.Template = template
	}
	if sort != 0 {
		goodsCate.Sort = sort
	}
	err5 := models.DB.Create(&goodsCate).Error
	if err5 != nil {
		c.ErrorJson(403, "新增数据失败", nil)
		return
	}
	c.SuccessJson(goodsCate)
}

// @Title Edit
// @Description 修改商品
// @Params id query int true "需要修改商品的id"
// @Params title query string false "分类的title"
// @Params pid query int false "分类id  0.顶级分类，其他 二级分类"
// @Params link query string  false "分类跳转地址"
// @Params template query string false "分类模板"
// @Params sub_title query string false  "seo标题"
// @Params keywords query string false "seo关键字"
// @Params description query string false "关键字描述"
// @Params sort query int false "状态"
// @Params cate_img query file false "分类图片"
// @Params status query int false "状态"
// @Success 200 {object} models.Manager
// @Failure 403 修改失败
// @router /goodsCate/edit [post]
func (c *GoodsCateController) Edit() {
	id, err1 := c.GetInt("id")
	title := c.GetString("title")
	pid, _ := c.GetInt("pid")
	link := c.GetString("link")
	template := c.GetString("template")
	sutTitle := c.GetString("sub_title")
	keywords := c.GetString("keywords")
	description := c.GetString("description")
	sort, _ := c.GetInt("sort")
	status, _ := c.GetInt("status")
	uploadDir, _ := c.UploadImg("cate_img")
	if err1 != nil {
		c.ErrorJson(403, "传入参数错误", nil)
		return
	}

	goodsCate := models.GoodsCate{Id: id}
	models.DB.Find(&goodsCate)
	if title != "" {
		goodsCate.Title = title
	}
	if pid != -1 {
		goodsCate.Pid = pid
	}
	if sort != -1 {
		goodsCate.Sort = sort
	}
	if uploadDir != "" {
		goodsCate.CateImg = uploadDir
	}
	if link != "" {
		goodsCate.Link = link
	}
	if sutTitle != "" {
		goodsCate.SubTitle = sutTitle
	}
	if keywords != "" {
		goodsCate.Keywords = keywords
	}
	if description != "" {
		goodsCate.Description = description
	}
	if template != "" {
		goodsCate.Template = template
	}
	goodsCate.Status = status
	err5 := models.DB.Save(&goodsCate).Error
	if err5 != nil {
		c.ErrorJson(503, "修改失败", nil)
		return
	}
	c.SuccessJson(goodsCate)
}

// @Title Delete
// @Description 删除商品
// @Params id query int true "要删除商品的id"
// @Success 200 {string} 删除成功
// @Failure 403 删除失败
// @router /goodsCate/delete [post]
func (c *GoodsCateController) Delete() {
	id, err := c.GetInt("id")
	if err != nil {
		c.ErrorJson(503, "传入参数错误", nil)
		return
	}
	GoodsCate := models.GoodsCate{Id: id}
	models.DB.Find(&GoodsCate)
	if GoodsCate.Pid == 0 {
		GoodsCate1 := []models.GoodsCate{}
		models.DB.Where("pid = ?", GoodsCate.Id).Find(&GoodsCate1)
		if len(GoodsCate1) > 0 {
			c.ErrorJson(503, "当前分类下面还有子分类，无法删除", nil)
			return
		}
	}
	GoodsCate2 := models.GoodsCate{Id: id}
	err1 := models.DB.Delete(GoodsCate2).Error
	if err1 != nil {
		c.ErrorJson(503, "删除失败", nil)
		return
	}
	c.SuccessJson("删除成功")
}
