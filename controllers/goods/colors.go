package goods

import (
	"fmt"
	"xmapi_demo/controllers"
	"xmapi_demo/models"
)

type ColorController struct {
	controllers.BaseController
}

// @Title Get
// @Description 获取所有颜色
// @Success 200 {object} models.GoodsColor
// @Failure 503 "获取错误"
// @router /getColor [post]
func (c *ColorController) Get() {
	fmt.Println("color")
	colors := []models.GoodsColor{}
	err := models.DB.Find(&colors).Error
	if err != nil {
		c.ErrorJson(403, "获取失败", nil)
		return
	}
	c.SuccessJson(colors)
}
