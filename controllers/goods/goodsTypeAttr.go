package goods

import (
	"strings"
	"xmapi_demo/controllers"
	"xmapi_demo/models"
)

type GoodsTypeAttrController struct {
	controllers.BaseController
}

// @Title Get
// @Description 获取分类数据
// @Params id query int false "分类id"
// @Success 200 {object} models.GoodsTypeAttribute
// @Failure 503 ""
// @router /goodsTypeAttr/get [post]
func (c *GoodsTypeAttrController) Get() {
	id, err := c.GetInt("id")
	if err != nil {
		c.ErrorJson(403, "传入参数错误", nil)
	}
	goodsTypeAttr := models.GoodsTypeAttribute{Id: id}
	err1 := models.DB.Find(&goodsTypeAttr).Error
	if err1 != nil {
		c.ErrorJson(403, "查询失败", nil)
		return
	}
	c.SuccessJson(goodsTypeAttr)
}

// @Title GetCate
// @Description 获取分类数据
// @Params cate_id query int true "商品类型属性id"
// @Success 200 {object} models.GoodsTypeAttribute
// @Failure 503 ""
// @router /goodsTypeAttr/getCate [post]
func (c *GoodsTypeAttrController) GetCate() {
	cateId, _ := c.GetInt("cate_id")

	goodsTypeAttr := []models.GoodsTypeAttribute{}
	err := models.DB.Preload("GoodsType").Where("cate_id = ?", cateId).Find(&goodsTypeAttr).Error
	if err != nil {
		c.ErrorJson(403, "查询失败", nil)
		return
	}
	c.SuccessJson(goodsTypeAttr)
}

// @Title Add
// @Description 获取分类数据
// @Params cate_id query int true "所属分类id"
// @Params title query string true "基本信息"
// @Params attr_type query int true "所属的类型 1.单行输入文本框 2.多行输入文本框 3.下拉文本框"
// @Params attr_value query string "商品类型的可选值"
// @Params status query int "状态"
// @params sort query int "排序"
// @Success 200 {object} models.GoodsTypeAttribute
// @Failure 403 "新增失败"
// @router /goodsTypeAttr/add [post]
func (c *GoodsTypeAttrController) Add() {
	title := c.GetString("title")
	cateId, err := c.GetInt("cate_id")
	attrType, err2 := c.GetInt("attr_type")
	attrValue := c.GetString("attr_value")
	status, _ := c.GetInt("status")
	sort, _ := c.GetInt("sort")
	if err != nil || err2 != nil {
		c.ErrorJson(403, "传入参数错误", nil)
		return
	}
	if strings.Trim(title, " ") == "" {
		c.ErrorJson(403, "标题不能为空", nil)
		return
	}
	goodsTypeAttr := models.GoodsTypeAttribute{
		Title:     title,
		CateId:    cateId,
		AttrType:  attrType,
		AttrValue: attrValue,
		Status:    status,
		Sort:      sort,
		AddTime:   int(models.GetUnix()),
	}
	err3 := models.DB.Create(&goodsTypeAttr).Error
	if err3 != nil {
		c.ErrorJson(403, "新增失败", nil)
		return
	}
	c.SuccessJson(goodsTypeAttr)
}

// @Title Edit
// @Description 获取分类数据
// @Params id query int false "分类id"
// @Params cate_id query int true "所属分类id"
// @Params title query string false "基本信息"
// @Params attr_type query int false "所属的类型 1.单行输入文本框 2.多行输入文本框 3.下拉文本框"
// @Params attr_value query false "商品类型的可选值"
// @Params status query int false "状态"
// @params sort query int false "排序"
// @Success 200 {object} models.GoodsTypeAttribute
// @Failure 503 ""
// @router /goodsTypeAttr/edit [post]
func (c *GoodsTypeAttrController) Edit() {
	id, _ := c.GetInt("id")
	title := c.GetString("title")
	cateId, err := c.GetInt("cate_id")
	attrType, err2 := c.GetInt("attr_type")
	attrValue := c.GetString("attr_value")
	status, _ := c.GetInt("status")
	sort, _ := c.GetInt("sort")
	if err != nil || err2 != nil {
		c.ErrorJson(403, "传入参数错误", nil)
		return
	}
	goodsTypeAttr := models.GoodsTypeAttribute{Id: id}
	models.DB.Find(&goodsTypeAttr)
	if title != "" {
		goodsTypeAttr.Title = title
	}
	if attrType != 0 {
		goodsTypeAttr.AttrType = attrType
	}
	if attrValue != "" && attrType == 3 {
		goodsTypeAttr.AttrValue = attrValue
	}
	goodsTypeAttr.Sort = sort
	goodsTypeAttr.Status = status
	goodsTypeAttr.CateId = cateId
	err4 := models.DB.Save(&goodsTypeAttr).Error
	if err4 != nil {
		c.ErrorJson(403, "修改失败", nil)
		return
	}
	c.SuccessJson(goodsTypeAttr)
}

// @Title Delete
// @Description 删除数据
// @Params id query int false "分类id"
// @Success 200 {object} models.GoodsTypeAttribute
// @Failure 503 ""
// @router /goodsTypeAttr/delete [post]
func (c *GoodsTypeAttrController) Delete() {
	id, err := c.GetInt("id")
	if err != nil {
		c.ErrorJson(403, "传入参数错误", nil)
		return
	}
	goodsTypeAttr := models.GoodsTypeAttribute{Id: id}
	err1 := models.DB.Delete(&goodsTypeAttr).Error
	if err1 != nil {
		c.ErrorJson(403, "删除数据失败", nil)
		return
	}
	c.SuccessJson("删除成功")
}
