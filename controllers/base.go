package controllers

import (
	"errors"
	"fmt"
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"github.com/astaxie/beego"
	"os"
	"path"
	"strconv"
	"xmapi_demo/models"
)

func (c *BaseController) UploadImg(picName string) (string, error) {
	//获取系统信息
	setting := models.Setting{}
	models.DB.First(&setting)
	ossStatus, _ := beego.AppConfig.Bool("ossStatus")
	ossDoMain := beego.AppConfig.String("ossDoMain")
	fmt.Println(setting)
	if ossStatus {
		url, err := c.OssUploadImg(picName)
		url = ossDoMain + url
		return url, err
	} else {
		return c.LocUploadImg(picName)
	}
}

//oss上传
func (c *BaseController) OssUploadImg(picName string) (string, error) {
	//获取系统信息
	setting := models.Setting{}
	models.DB.First(&setting)
	//获取上传的文件
	f, h, err := c.GetFile(picName)
	if err != nil {
		return "", err
	}
	//关闭文件流
	defer f.Close()
	//3.获取后缀名，判断类型是否正确 .jpg .png .gif .jpeg
	extName := path.Ext(h.Filename)
	allowExtMap := map[string]bool{
		".jpg":  true,
		".png":  true,
		".gif":  true,
		".jpeg": true,
	}
	if _, ok := allowExtMap[extName]; !ok {
		return "", errors.New("图片后缀名不合法")
	}
	// 创建OSSClient实例。
	client, err := oss.New("oss-cn-beijing.aliyuncs.com", "LTAI4G97XeMaBf6AYeUu2wch", "p6LtwQ9hgKXTnMeg0B9XQIjsp0NJIx")
	if err != nil {
		return "", err
	}

	// 获取存储空间。
	bucket, err := client.Bucket("tjlbucket")
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(-1)
	}
	day := models.GetDay()
	dir := "static/upload" + day
	//5.生成文件名称
	fileUnixName := strconv.FormatInt(models.GetUnix(), 10)
	saveDir := path.Join(dir, fileUnixName+extName)
	// 上传文件流。
	err = bucket.PutObject(saveDir, f)
	fmt.Println(err, "上传错误")
	if err != nil {
		return "", err
	}
	return saveDir, nil
}

func (c *BaseController) LocUploadImg(picName string) (string, error) {
	//获取上传的文件
	f, h, err := c.GetFile(picName)
	if err != nil {
		return "", err
	}
	//关闭文件流
	defer f.Close()
	//3.获取后缀名，判断类型是否正确 .jpg .png .gif .jpeg
	extName := path.Ext(h.Filename)
	allowExtMap := map[string]bool{
		".jpg":  true,
		".png":  true,
		".gif":  true,
		".jpeg": true,
	}
	if _, ok := allowExtMap[extName]; !ok {
		return "", errors.New("图片后缀名不合法")
	}
	//4.创建图片保存目录 static/upload
	day := models.GetDay()
	dir := "static/upload" + day
	if err := os.MkdirAll(dir, 0666); err != nil {
		return "", err
	}
	//5.生成文件名称
	fileUnixName := strconv.FormatInt(models.GetUnix(), 10) + extName
	saveDir := path.Join(dir, fileUnixName)
	c.SaveToFile(picName, saveDir)
	return saveDir, nil
}

func (c *BaseController) GetSetting() models.Setting {
	setting := models.Setting{Id: 1}
	models.DB.Find(&setting)
	return setting
}
func (c *BaseController) UploadImgs(picName string) ([]string, error) {
	return c.LocUploadImgs(picName)
}

func (c *BaseController) LocUploadImgs(picName string) ([]string, error) {
	// 获取上传的文件
	files, err := c.GetFiles(picName)
	if err != nil {
		c.ErrorJson(403, "上传图片失败", nil)
		return []string{}, err
	}
	allowExtMap := map[string]bool{
		".jpg":  true,
		".png":  true,
		".gif":  true,
		".jpeg": true,
	}
	var file = []string{}
	for i, _ := range files {
		extName := path.Ext(files[i].Filename)
		if _, ok := allowExtMap[extName]; !ok {
			return []string{}, errors.New("图片后缀名不合法")
		}
		//4.创建图片保存目录 static/upload
		day := models.GetDay()
		dir := "static/upload" + day
		if err := os.MkdirAll(dir, 0666); err != nil {
			return []string{}, err
		}
		fileUnixName := strconv.FormatInt(models.GetUnixNano(), 10) + extName
		saveDir := path.Join(dir, fileUnixName)
		fmt.Printf("%#v", picName)
		c.SaveToFile(picName, saveDir)
		file = append(file, saveDir)
	}
	return file, err
}
