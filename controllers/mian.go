package controllers

import (
	"xmapi_demo/models"
)

type MainController struct {
	BaseController
}

// @Title ChangeStatus
// @Description 公共更新方法
// @Params id query  int true "需要更新的id"
// @Params table query string true "需要更新的表"
// @Params filed query string true "需要更新的字段"
// @Success 200 {string} 数据更新成功
// @Failure 403 数据更新失败
// @router /changeStatus [post]
func (c *MainController) ChangeStatus() {
	id, err := c.GetInt("id")
	if err != nil {
		c.ErrorJson(403, "传入参数错误", nil)
		return
	}
	table := c.GetString("table")
	filed := c.GetString("filed")
	err2 := models.DB.Exec("update "+table+" set "+filed+"=ABS("+filed+" - 1) where id = ?", id).Error
	if err2 != nil {
		//beego.Error("错误")
		c.ErrorJson(403, "数据更新失败", nil)
		return
	}
	c.SuccessJson("数据更新成功")
}

// @Title EditNum
// @Description 公共更新方法
// @Params id query  int true "需要修改的id"
// @Params table query string true "需要修改的表"
// @Params filed query string true "需要修改的字段"
// @Params num query string true "需要修改的值"
// @Success 200 {string} 数据更新成功
// @Failure 403 数据更新失败
// @router /changeNum [post]
func (c *MainController) EditNum() {
	id, err := c.GetInt("id")
	if err != nil {
		c.ErrorJson(403, "传入参数错误", nil)
		return
	}
	table := c.GetString("table")
	filed := c.GetString("filed")
	num := c.GetString("num")
	err2 := models.DB.Exec("update "+table+" set "+filed+"="+"'"+num+"'"+" where id = ?", id).Error
	if err2 != nil {
		c.ErrorJson(403, "数据修改失败", nil)
		return
	}
	c.SuccessJson("数据修改成功")
}
