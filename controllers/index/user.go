package index

import (
	"xmapi_demo/controllers"
	"xmapi_demo/models"
)

type UserController struct {
	controllers.BaseController
}

// @Title Get
// @Description 用户
// @Success 200 {string} "上传成功"
// @Failure 503 ""
// @router /get [post]
func (c *UserController) Get() {

	nav := []models.Nav{}
	models.DB.Find(&nav)
	goods := []models.Goods{}
	models.DB.Limit(10).Offset(1).Find(&goods)
	list := map[string]interface{}{
		"nav":   nav,
		"goods": goods,
	}
	c.SuccessJson(list)
}
