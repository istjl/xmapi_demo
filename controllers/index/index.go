package index

import (
	"github.com/astaxie/beego"
	"xmapi_demo/models"

	//. "github.com/hunterhug/go_image"
	qrucode "github.com/skip2/go-qrcode"
	"xmapi_demo/controllers"
)

type IndexController struct {
	controllers.BaseController
}

// @Title Get
// @Description 图片
// @Success 200 {string} ""
// @Failure 503 ""
// @router /index/index [post]
func (c *IndexController) Get() {

	//实现图片剪切 按宽度进行比例缩放，输入输出都是
	//filename := "static/a.jpg"
	//savepath := "static/a_800.jpg"
	//err := ScaleF2F(filename, savepath, 100)

	//filename := "static/a.jpg"
	//savepath := "static/a_800_300.jpg"
	//err := ThumbnailF2F(filename, savepath, 300, 400)
	//
	//if err != nil {
	//	beego.Error(err)
	//}
	models.ResizeImage([]string{})
	err := qrucode.WriteFile("http://www.baidu.com", qrucode.Medium, 256, "static/qr.png")
	if err != nil {
		beego.Info(err)
	}
}
