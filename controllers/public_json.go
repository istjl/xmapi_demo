package controllers

import "github.com/astaxie/beego"

type BaseController struct {
	beego.Controller
}
type ReturnMsg struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

func (c *BaseController) SuccessJson(data interface{}) {
	res := ReturnMsg{
		200, "success", data,
	}
	c.Data["json"] = res
	c.ServeJSON()
	c.StopRun()
}
func (c *BaseController) ErrorJson(code int, msg string, data interface{}) {
	res := ReturnMsg{
		code, msg, data,
	}
	c.Data["json"] = res
	c.ServeJSON()
	c.StopRun()
}
