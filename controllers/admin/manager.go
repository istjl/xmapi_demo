package admin

import (
	"strings"
	"xmapi_demo/controllers"
	"xmapi_demo/models"
)

type ManagerController struct {
	controllers.BaseController
}

// @Title Get
// @Description 获取管理员信息
// @Params id query int false "管理员id"
// @Success 200 {object} models.Manager
// @Failure 403 查询错误
// @router /manager/get [post]
func (c *ManagerController) Get() {
	id, _ := c.GetInt("id")

	if id != 0 {
		manager := models.Manager{Id: id}
		err := models.DB.Preload("Role").Find(&manager).Error
		if err != nil {
			c.ErrorJson(403, "非法请求", nil)
			return
		} else {
			c.SuccessJson(manager)
			return
		}
	}
	manager := []models.Manager{}
	err := models.DB.Preload("Role").Find(&manager).Error
	if err != nil {
		c.ErrorJson(403, "查询错误", nil)
		return
	} else {
		c.SuccessJson(manager)
	}
}

// @Title Add
// @Description 新增管理员
// @Params role_id query string true "角色id"
// @Params username query string true "管理员名称"
// @Params password query string true "管理员密码"
// @Params mobile query string true "管理员电话"
// @Params email query string true "管理员邮箱"
// @Success 200 {object} models.Manager
// @Failure 403 删除角色失败
// @router /manager/add [post]
func (c *ManagerController) Add() {
	username := strings.Trim(c.GetString("username"), "")
	password := strings.Trim(c.GetString("password"), "")
	moblie := strings.Trim(c.GetString("mobile"), "")
	email := strings.Trim(c.GetString("email"), "")
	roleId, err1 := c.GetInt("role_id")
	if err1 != nil {
		c.ErrorJson(503, "非法请求", nil)
	}
	if len(username) < 2 || len(password) < 6 {
		c.ErrorJson(503, "用户名或密码长度不合法", nil)
		return
	}
	//判断数据库是否有当前用户
	mangerList := []models.Manager{}
	models.DB.Where("username = ?", username).Find(&mangerList)
	if len(mangerList) > 0 {
		c.ErrorJson(503, "用户名已存在", nil)
		return
	}
	manager := models.Manager{
		Username: username,
		Password: models.Md5(password),
		Mobile:   moblie,
		Email:    email,
		IsSuper:  0,
		Status:   1,
		AddTime:  int(models.GetUnix()),
		RoleId:   roleId,
	}
	err := models.DB.Create(&manager).Error
	if err != nil {
		c.ErrorJson(503, "增加管理员失败", nil)
		return
	} else {
		c.SuccessJson(manager)
	}

}

// @Title Edit
// @Description 修改权限
// @Params id query string true  "用户id"
// @Params role_id query string false "角色id"
// @Params password query string false "管理员密码"
// @Params mobile query string false "管理员电话"
// @Params email query string false "管理员邮箱"
// @Success 200 {object} models.Manager
// @Failure 403 修改失败
// @router /manager/edit [post]
func (c *ManagerController) Edit() {
	id, err1 := c.GetInt("id")
	if err1 != nil {
		c.ErrorJson(403, "非法请求", nil)
		return
	}
	roleId, _ := c.GetInt("role_id")
	password := strings.Trim(c.GetString("password"), "")
	moblie := strings.Trim(c.GetString("mobile"), "")
	email := strings.Trim(c.GetString("email"), "")
	//获取数据
	manager := models.Manager{Id: id}
	models.DB.Find(&manager)
	if roleId != 0 {
		manager.RoleId = roleId
	}
	if moblie != "" {
		manager.Mobile = moblie
	}
	if email != "" {
		manager.Email = email
	}
	if password != "" {
		if len(password) < 6 {
			c.ErrorJson(403, "密码长度不合法,密码长度不能小于6位", nil)
			return
		}
		manager.Password = models.Md5(password)
	}
	//执行修改
	err3 := models.DB.Save(&manager).Error
	if err3 != nil {
		c.ErrorJson(403, "修改失败", nil)
		return
	} else {
		c.SuccessJson(manager)
	}
}

// @Title Delete
// @Description 修改权限
// @Params id query int true "删除角色id"
// @Success 200 {string} 删除成功
// @Failure 403 删除失败
// @router /manager/delete [post]
func (c *ManagerController) Delete() {
	id, err := c.GetInt("id")
	if err != nil {
		c.ErrorJson(403, "非法请求", nil)
		return
	}
	manager := models.Manager{Id: id}
	err1 := models.DB.Delete(&manager).Error
	if err1 != nil {
		c.ErrorJson(403, "删除管理员失败", nil)
		return
	}
	c.SuccessJson("删除管理员成功")
}
