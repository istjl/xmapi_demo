package admin

import (
	"encoding/json"
	"fmt"
	"xmapi_demo/controllers"
	"xmapi_demo/models"
)

type CaptchaController struct {
	controllers.BaseController
}

// @Title Get
// @Description 验证码
// @Success 200 {string} code
// @Failure 403 获取验证码错误
// @router /captcha [get]
func (c *CaptchaController) Get() {
	id, b64s, err := models.GetCaptcha()
	if err != nil {
		c.ErrorJson(403, "获取图形验证码错误", nil)
		return
	}
	c.SuccessJson(models.Captcha{id, b64s})
}

// @Title Post
// @Description 验证码
// @Success 200 {string} code
// @Failure 403 获取验证码错误
// @router /captcha [post]
func (c *CaptchaController) Post() {
	var captcha models.Captcha
	fmt.Println(c.Ctx.Input.RequestBody)
	json.Unmarshal(c.Ctx.Input.RequestBody, &captcha)
	if models.VerifyCaptcha(captcha.ID, captcha.B64s) {
		c.SuccessJson("true")
		return
	}
	c.ErrorJson(403, "验证码错误", nil)
}
