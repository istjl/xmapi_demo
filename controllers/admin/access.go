package admin

import (
	"xmapi_demo/controllers"
	"xmapi_demo/models"
)

type AccessController struct {
	controllers.BaseController
}

// @Title Get
// @Description 获取顶级模块
// @Success 200 {object} models.Access
// @Failure 403 获取顶级模块失败
// @router /access/get [get]
func (c *AccessController) Get() {
	access := []models.Access{}
	err := models.DB.Where("module_id = 0").Find(&access).Error
	if err != nil {
		c.ErrorJson(503, "获取顶级模块失败", nil)
		return
	}
	c.SuccessJson(access)
}

// @Title GetList
// @Description 获取关联数据
// @Success 200 {object} models.Access
// @Failure 403 获取数据失败
// @router /access/getlist [get]
func (c *AccessController) GetList() {
	access := []models.Access{}
	err := models.DB.Preload("AccessItem").Where("module_id = 0").Find(&access).Error
	if err != nil {
		c.ErrorJson(403, "获取数据失败", nil)
		return
	}
	c.SuccessJson(access)
}

// @Title Add
// @Description 新增管理员
// @Params module_name query string true "模块名称"
// @Params type query int true "节点类型 1表示模块 2表示菜单 3操作"
// @Params action_name query string  false "操作类型"
// @Params url query string false "跳转地址"
// @Params module_id query string false  "此module_id 和当前模型的_id 关联    module_id 0 表示模块"
// @Params sort query int true "排序"
// @Params description query string true "描述"
// @Params status query int true "状态"
// @Success 200 {object} models.Access
// @Failure 403 增加数据失败
// @router /access/add [post]
func (c *AccessController) Add() {
	moduleName := c.GetString("module_name")
	iType, err1 := c.GetInt("type")
	actionName := c.GetString("action_name")
	url := c.GetString("url")
	moduleId, _ := c.GetInt("module_id")
	sort, err3 := c.GetInt("sort")
	description := c.GetString("description")
	status, err4 := c.GetInt("status")
	if err1 != nil || err3 != nil || err4 != nil {
		c.ErrorJson(403, "传入参数错误", nil)
		return
	}
	access := models.Access{
		ModuleName:  moduleName,
		Type:        iType,
		Sort:        sort,
		Description: description,
		Status:      status,
	}
	if actionName != "" {
		access.ActionName = actionName
	}
	if url != "" {
		access.Url = url
	}
	if moduleId != 0 {
		access.ModuleId = moduleId
	}

	err := models.DB.Create(&access).Error
	if err != nil {
		c.ErrorJson(403, "增加数据失败", nil)
		return
	}
	c.SuccessJson(access)
}

// @Title Edit
// @Description 修改权限
// @Params id query string true  "用户id"
// @Params role_id query string false "角色id"
// @Params password query string false "管理员密码"
// @Params mobile query string false "管理员电话"
// @Params email query string false "管理员邮箱"
// @Success 200 {object} models.Manager
// @Failure 403 修改失败
// @router /access/edit [post]
func (c *AccessController) Edit() {

}

// @Title Delete
// @Description 修改权限
// @Params id query int true "删除角色id"
// @Success 200 {string} 删除成功
// @Failure 403 删除失败
// @router /access/delete [post]
func (c *AccessController) Delete() {
	id, err := c.GetInt("id")
	if err != nil {
		c.ErrorJson(403, "传入参数错误", nil)
		return
	}
	access := models.Access{Id: id}
	models.DB.Find(&access)
	if access.ModuleId == 0 {
		access1 := []models.Access{}
		models.DB.Where("module_id = ?", access.Id).Find(&access1)
		if len(access1) > 0 {
			c.ErrorJson(403, "需要删除子模块后删除顶级模块", nil)
			return
		}
	}
	err1 := models.DB.Delete(&access).Error
	if err1 != nil {
		c.ErrorJson(403, "删除失败", nil)
		return
	}
	c.SuccessJson("删除成功")

}
