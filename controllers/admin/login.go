package admin

import (
	"fmt"
	"github.com/astaxie/beego/cache"
	"github.com/astaxie/beego/utils/captcha"
	"strings"
	"xmapi_demo/controllers"
	"xmapi_demo/models"
)

type LoginControllers struct {
	controllers.BaseController
}

var Cpt *captcha.Captcha

func init() {
	store := cache.NewMemoryCache()
	Cpt = captcha.NewWithFilter("/captcha", store)
}

// @Title Captcha
// @Description 验证码
// @Success 200 {string} code
// @Failure 403 获取验证码错误
// @router /captcha [post]
func (c *LoginControllers) CaptCha() {
	num, err := Cpt.CreateCaptcha()
	if err != nil {
		c.ErrorJson(500, err.Error(), nil)
		return
	}
	c.SuccessJson(num)
}

// @Title Login
// @Description 登录
// @Params username query string true "账号"
// @Params password query string true "密码"
// @Params code query string true
// @Success 200 {object} models.Mnager
// @Failure 403 账号或密码错误
// @router /login [post]
func (c *LoginControllers) Login() {
	/*
		1、验证用户输入的验证码是否正确
		2、获取表单传过来的用户名和密码
		3、去数据库匹配
			1、保存用户信息
	*/
	var flag = Cpt.VerifyReq(c.Ctx.Request)
	if flag {
		c.ErrorJson(500, "登录失败", nil)
	} else {
		username := strings.Trim(c.GetString("username"), "")
		password := models.Md5(strings.Trim(c.GetString("password"), ""))
		fmt.Println(username, password)
		manager := models.Manager{}
		err2 := models.DB.Where("username = ? and password = ?", username, password).Find(&manager).Error
		if err2 != nil {
			fmt.Println(err2)
			c.ErrorJson(403, "账号或密码错误", nil)
			return
		}
		c.SetSession("userInfo", manager)
		c.SuccessJson(manager)
	}
}

// @Title Register
// @Description 注册
// @Params username query string true "账号"
// @Params password query string true "密码"
// @Params moblie query int true  "手机号"
// @Params email query string false "邮箱"
// @Success 200 {bool} 成功
// @Failure 403 用户已存在
// @router /register [post]
func (c *LoginControllers) Register() {
	username := strings.Trim(c.GetString("username"), "")
	password := models.Md5(strings.Trim(c.GetString("password"), ""))
	moblie := strings.Trim(c.GetString("moblie"), "")
	email := strings.Trim(c.GetString("email"), "")
	add_time := int(models.GetUnix())
	fmt.Println(password)
	user := []models.Manager{}
	models.DB.Where("username = ? ", username).Find(&user)
	if (len(user)) > 0 {
		c.ErrorJson(500, "用户已注册", nil)
	} else {
		userinfo := models.Manager{
			Username: username,
			Password: password,
			Mobile:   moblie,
			Email:    email,
			AddTime:  add_time,
		}
		create := models.DB.Create(&userinfo)
		if create != nil {
			c.SuccessJson(userinfo)
		} else {
			c.ErrorJson(404, "注册失败", nil)
		}
	}

}

// @Title LoginOut
// @Description 注册
// @Success 200 {string} 退出成功
// @Failure 403
// @router /loginOut [post]
func (c *LoginControllers) LoginOut() {
	c.DelSession("userInfo")
	c.SuccessJson("退出成功")
}
