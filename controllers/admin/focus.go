package admin

import (
	"xmapi_demo/controllers"
	"xmapi_demo/models"
)

type FocusController struct {
	controllers.BaseController
}

// @Title Get
// @Description 获取轮播图
// @Success 200 {object} models.Focus
// @Failure 403 获取图片失败
// @router /focus/get [get]
func (c *FocusController) Get() {
	focus := []models.Focus{}
	err := models.DB.Find(&focus).Error
	if err != nil {
		c.ErrorJson(403, "获取图片失败", nil)
		return
	}
	c.SuccessJson(focus)
}

// @Title Add
// @Description 新增轮播图
// @Params focus_img query  string false "上传轮播图文件"
// @Params focus_type query  int true "轮播图类型"
// @Params title query  string true "轮播图名字"
// @Params link query  string true "跳转地址"
// @Params sort query  int true "顺序"
// @Params status query  int true "轮播图状态"
// @Success 200 {object} models.Focus
// @Failure 403 新增失败失败
// @router /focus/add [post]
func (c *FocusController) Add() {
	focusType, err1 := c.GetInt("focus_type")
	title := c.GetString("title")
	link := c.GetString("link")
	sort, err2 := c.GetInt("sort")
	status, err3 := c.GetInt("status")
	if err1 != nil || err2 != nil || err3 != nil {
		c.ErrorJson(403, "传入参数错误", nil)
		return
	}
	//执行图片上传
	focusImg := c.GetString("focus_img")
	focus := models.Focus{
		Title:     title,
		FocusType: focusType,
		FocusImg:  focusImg,
		Link:      link,
		Sort:      sort,
		Status:    status,
		AddTime:   int(models.GetUnix()),
	}
	err6 := models.DB.Save(&focus).Error
	if err6 != nil {
		c.ErrorJson(403, "上传文件失败", nil)
		return
	}
	c.SuccessJson(focus)
}

// @Title Edit
// @Description 修改轮播图
// @Params id query int true "需要修改的id"
// @Params focus_img query  file false "上传轮播图文件"
// @Params focus_type query  int false "轮播图类型"
// @Params title query  string false "轮播图名字"
// @Params link query  string false "跳转地址"
// @Params sort query  int false "顺序"
// @Params status query  int false "轮播图状态"
// @Success 200 {object} models.Focus
// @Failure 403 获取图片失败
// @router /focus/edit [get]
func (c *FocusController) Edit() {
	id, err := c.GetInt("id")
	if err != nil {
		c.ErrorJson(403, "传入参数错误", nil)
		return
	}
	focus := models.Focus{Id: id}
	models.DB.Find(&focus)
	c.ParseForm(&focus)
	err2 := models.DB.Save(&focus).Error
	if err2 != nil {
		c.ErrorJson(403, "保存失败", nil)
		return
	}
	c.SuccessJson(focus)
}

// @Title Delete
// @Description 删除轮播图
// @Params id query int true "需要删除的id "
// @Success 200 {string} "删除成功"
// @Failure 403 删除图片失败
// @router /focus/delete [post]
func (c *FocusController) Delete() {
	id, err := c.GetInt("id")
	if err != nil {
		c.ErrorJson(403, "传入参数错误", nil)
		return
	}
	focus := models.Focus{Id: id}
	err1 := models.DB.Delete(&focus).Error
	if err1 != nil {
		c.ErrorJson(403, "删除失败", nil)
		return
	}
	c.SuccessJson("删除成功")

}
