package admin

import (
	"fmt"
	"xmapi_demo/controllers"
	"xmapi_demo/models"
)

type SettingController struct {
	controllers.BaseController
}

// @Title Get
// @Description 获取setting数据
// @Success 200 {object} models.Setting
// @Failure 503 "获取数据失败"
// @router /setting/get [post]
func (c *SettingController) Get() {
	setting := models.Setting{}
	models.DB.First(&setting)
	c.SuccessJson(setting)
}

// @Title Edit
// @Description 获取nav的数据
// @Params id query int true "id"
// @Params site_title query string false "网站标题"
// @Params site_logo query file false "网站logo"
// @Params site_keywords query string false "网站关键词"
// @Params site_description query string false "网站描述"
// @Params no_picture query file false "加载不出图片"
// @Params site_icp query string false "备案信息"
// @Params site_tel query string false "客服电话"
// @Params search_keywords query string false "搜索关键词"
// @Params tongji_code query string false "统计代码"
// @Params appid query string false "appid"
// @Params app_secret query string false ""
// @Params end_point query string false ""
// @Params bucket_name query string false "名字"
// @Params oss_status query int false "oss状态"
// @Success 200 {object} models.Nav
// @Failure 503 "获取数据失败"
// @router /setting/edit [post]
func (c *SettingController) Edit() {
	id, _ := c.GetInt("id")
	setting := models.Setting{Id: id}
	models.DB.Find(&setting)
	c.ParseForm(&setting)
	site_logo, err := c.UploadImg("site_logo")
	if len(site_logo) > 0 && err == nil {
		setting.SiteLogo = site_logo
	}
	no_picture, err1 := c.UploadImg("no_picture")
	if len(no_picture) > 0 && err1 == nil {
		setting.NoPicture = no_picture
	}
	err3 := models.DB.Save(&setting).Error
	if err3 != nil {
		c.ErrorJson(403, "修改失败", nil)
		return
	}
	c.SuccessJson(setting)
	fmt.Printf("%v", setting)
}
