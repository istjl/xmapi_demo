package admin

import (
	"fmt"
	"strconv"
	"strings"
	"xmapi_demo/controllers"
	"xmapi_demo/models"
)

type RoleController struct {
	controllers.BaseController
}

// @Title Get
// @Description 获取角色的数据
// @Params id query int false "角色id"
// @Success 200 {object} models.Role
// @Failure 503 ""
// @router /role/get [post]
func (c *RoleController) Get() {
	id, _ := c.GetInt("id")
	roles := []models.Role{}
	fmt.Println(id)
	if id != 0 {
		role := models.Role{Id: id}
		models.DB.Find(&role)
		c.SuccessJson(role)
	} else {
		err := models.DB.Find(&roles).Error
		if err != nil {
			c.ErrorJson(503, "获取角色失败", nil)
		} else {
			c.SuccessJson(roles)
		}
	}
}

// @Title Add
// @Description 增加角色
// @Params title query string true "增加角色的title"
// @Params description query string true "增加角色的描述"
// @Success 200 {string} 增加角色成功
// @Failure 503 增加角色失败
// @router /role/add [post]
func (c *RoleController) Add() {
	title := strings.Trim(c.GetString("title"), "")
	description := strings.Trim(c.GetString("description"), "")

	if title == "" {
		c.ErrorJson(503, "标题不能为空", nil)
		return
	}
	role := models.Role{}
	role.Title = title
	role.Description = description
	role.Status = 1
	role.AddTime = int(models.GetUnix())
	err := models.DB.Create(&role).Error
	if err != nil {
		c.ErrorJson(503, "增加角色失败", nil)
	} else {
		c.SuccessJson("增加角色成功")
	}
}

// @Title Edit
// @Description 编辑角色
// @Params id query int true "编辑角色id"
// @Params title query string true "编辑角色title"
// @Params description query string true "编辑角色的描述"
// @Success 200 {object} models.Role
// @Failure 503 修改角色失败
// @router /role/edit [post]
func (c *RoleController) Edit() {
	id, err := c.GetInt("id")
	if err != nil {
		c.ErrorJson(503, "传入参数错误", nil)
		return
	}
	title := c.GetString("title")
	description := c.GetString("description")
	if title == "" {
		c.ErrorJson(503, "传入参数错误", nil)
		return
	}
	role := models.Role{Id: id}
	models.DB.Find(&role)
	role.Title = title
	role.Description = description

	err1 := models.DB.Save(&role).Error
	if err1 != nil {
		c.ErrorJson(503, "修改数据失败", nil)
		return
	} else {
		c.SuccessJson(role)
	}
}

// @Title Delete
// @Description 删除角色权限
// @Params id query string true "删除角色id"
// @Success 200 {string} 删除成功
// @Failure 403 删除角色失败
// @router /role/delete [post]
func (c *RoleController) Delete() {
	id, err := c.GetInt("id")
	role := models.Role{Id: id}
	if err != nil {
		c.ErrorJson(503, "传入参数错误", nil)
		return
	} else {
		err := models.DB.Delete(&role).Error
		if err != nil {
			c.ErrorJson(403, "删除角色失败", nil)
		} else {
			c.SuccessJson("删除成功")
		}
	}
}

// @Title Auth
// @Description 修改角色授权
// @Params id query string true "修改角色id"
// @Params access_node query Array[int] false "修改权限"
// @Success 200 {object} models.RoleAccess
// @Failure 403 删除角色失败
// @router /role/auth [post]
func (c *RoleController) Auth() {
	id, err := c.GetInt("id")
	if err != nil {
		c.ErrorJson(503, "传入参数错误", nil)
		return
	}
	accessNode := c.GetString("access_node")
	str := strings.Split(accessNode, ",")
	roleAccess := models.RoleAccess{}
	models.DB.Where("role_id = ? ", id).Delete(&roleAccess)
	for _, v := range str {
		fmt.Println(v)
		accessId, _ := strconv.Atoi(v)
		roleAccess.AccessId = accessId
		roleAccess.RoleId = id
		models.DB.Create(&roleAccess)
	}
	c.SuccessJson(roleAccess)
}

// @Title GetAuth
// @Description 获取该角色的权限
// @Params id query string true "角色id"
// @Success 200 {object} models.RoleAccess
// @Failure 403 删除角色失败
// @router /role/getAuth [post]
func (c *RoleController) GetAuth() {
	id, err := c.GetInt("id")
	if err != nil {
		c.ErrorJson(403, "传入参数错误", nil)
		return
	}
	access := []models.Access{}
	models.DB.Preload("AccessItem").Where("module_id = 0").Find(&access)
	roleAccess := []models.RoleAccess{}
	models.DB.Where("role_id = ?", id).Find(&roleAccess)

	roleAccessMap := make(map[int]int)
	for _, v := range roleAccess {
		roleAccessMap[v.AccessId] = v.AccessId
	}
	for i := 0; i < len(access); i++ {
		_, ok := roleAccessMap[access[i].Id]
		if ok {
			access[i].Checked = true
		}
		for j := 0; j < len(access[i].AccessItem); j++ {
			if _, ok := roleAccessMap[access[i].AccessItem[j].Id]; ok {
				access[i].AccessItem[j].Checked = true
			}
		}
	}
	c.SuccessJson(access)
}
