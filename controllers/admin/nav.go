package admin

import (
	"fmt"
	"xmapi_demo/controllers"
	"xmapi_demo/models"
)

type NavController struct {
	controllers.BaseController
}

// @Title Get
// @Description 获取nav的数据
// @Params position query int false "定位值"
// @Success 200 {object} models.Nav
// @Failure 503 "获取数据失败"
// @router /nav/get [post]
func (c *NavController) Get() {
	position, _ := c.GetInt("position")
	nav := []models.Nav{}
	fmt.Println(position)
	if position != 0 {
		err2 := models.DB.Where("position = ?", position).Find(&nav).Error
		if err2 != nil {
			c.ErrorJson(403, "获取数据失败", nil)
			return
		}
		c.SuccessJson(nav)
		return
	}
	err := models.DB.Find(&nav).Error
	if err != nil {
		c.ErrorJson(403, "查询失败", nil)
		return
	}
	c.SuccessJson(nav)

}

// @Title Add
// @Description 新增nav
// @Params title query string true "nav标题"
// @Params link query string true "跳转地址"
// @Params position query int true "定位"
// @Params is_opennew query int true "是否跳转新链接"
// @Params relation query string false "链接商品"
// @Params sort query int true "排序"
// @Params status query int true "状态"
// @Success 200 {object} models.Nav
// @Failure 503 "获取数据失败"
// @router /nav/add [post]
func (c *NavController) Add() {
	title := c.GetString("title")
	link := c.GetString("link")
	position, _ := c.GetInt("position")
	is_opennew, _ := c.GetInt("is_opennew")
	relation := c.GetString("relation")
	sort, _ := c.GetInt("sort")
	status, _ := c.GetInt("status")
	nav := models.Nav{
		Title:     title,
		Link:      link,
		Position:  position,
		IsOpennew: is_opennew,
		Sort:      sort,
		Status:    status,
		Relation:  relation,
		AddTime:   int(models.GetUnix()),
	}
	err := models.DB.Create(&nav).Error
	if err != nil {
		c.ErrorJson(403, "新增nav失败", nil)
		return
	}
	c.SuccessJson(nav)
}

// @Title Edit
// @Description 修改nav数据
// @Params id query int true "需要修改nav的id"
// @Params title query string false "nav标题"
// @Params link query string false "跳转地址"
// @Params position query int false "定位"
// @Params is_opennew query int false "是否跳转新链接"
// @Params relation query string false "链接商品"
// @Params sort query int false "排序"
// @Params status query int false "状态"
// @Success 200 {object} models.Nav
// @Failure 503 "获取数据失败"
// @router /nav/edit [post]
func (c *NavController) Edit() {
	id, err := c.GetInt("id")
	if err != nil {
		c.ErrorJson(403, "传入参数错误", nil)
		return
	}
	title := c.GetString("title")
	link := c.GetString("link")
	position, _ := c.GetInt("position")
	is_opennew, _ := c.GetInt("is_opennew")
	relation := c.GetString("relation")
	sort, _ := c.GetInt("sort")
	status, _ := c.GetInt("status")
	nav := models.Nav{Id: id}
	models.DB.Find(&nav)
	fmt.Println(is_opennew)
	if title != "" {
		nav.Title = title
	}
	if link != "" {
		nav.Link = link
	}
	if position != 0 {
		nav.Position = position
	}
	if is_opennew != 0 {
		nav.IsOpennew = is_opennew
	}
	if sort != 0 {
		nav.Sort = sort
	}
	if status != 0 {
		nav.Status = status
	}
	if relation != "" {
		nav.Relation = relation
	}
	err1 := models.DB.Save(&nav).Error
	if err1 != nil {
		c.ErrorJson(403, "修改失败", nil)
		return
	}
	c.SuccessJson(nav)
}

// @Title Delete
// @Description 删除数据
// @Params id query int false "nav的id"
// @Success 200 {object} "删除成功"
// @Failure 503 "获取数据失败"
// @router /nav/delete [post]
func (c *NavController) Delete() {
	id, err := c.GetInt("id")
	if err != nil {
		c.ErrorJson(403, "传入参数错误", nil)
		return
	}
	nav := models.Nav{Id: id}
	err1 := models.DB.Delete(&nav).Error
	if err1 != nil {
		c.ErrorJson(403, "删除失败", nil)
		return
	}
	c.SuccessJson("删除成功")
}
